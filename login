<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>SignUp</title>
  <link rel="icon" type="image/x-icon" href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnj5YFv4eZArpFStaSGIXoHJ4lAesatFGwYA&usqp=CAU">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <style>
  .container{
    height:500px;
    width:100%;
  }
  #heading{
    font-family: 'Google Sans','Noto Sans Myanmar UI',arial,sans-serif;
    font-size: 24px;
    font-weight: 400;
  }
  body{
    margin:0;
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    color:#3d3d3d;
}

.form-container{
    width:375px;
    height:450px;
    margin:auto;
    position: relative;
    padding:40px;
    border:1px solid blue;
    border-radius: 5px;
    overflow: hidden;
    box-shadow: 1px 1px 13px 1px #b7b7b7;
}
.form-group{
    position: relative;
}
.form-group input{
    width:100%;
    padding:12px 10px;
    box-sizing: border-box;
    border-radius: 4px;
    border:1px solid blue;
    background-color: transparent;
    font-size: 16px;
}
.form-group label{
    position: absolute;
    left:20px;
    top:9px;
    color:#a5a5a5;
    font-size:16px;
    padding:0px 4px;
    background-color: white;
    transition-property: transform;
    transition-duration: 500ms;
    z-index:-1;
}
.form-link{
    padding:4px 0px;
}
.form-link a{
    color:#1a73e8;
    font-weight: 500;
}
.form-link label{
    color:blue;
    font-size: 14px;
}
#email:focus + label{
    transform: translate3d(0, -100%, 0);
    z-index:1;
    color:#1a73e8;
}
#password:focus + label{
    transform: translate3d(0, -100%, 0);
    z-index:1;
    color:blue;
}
.password-fixed{
    transform: translate3d(0, -100%, 0);
    z-index:1 !important;
    color:#1a73e8;
}
.form-header{
    text-align: center;
}
.form-header h2{
    margin:0;
    font-weight:600;
}
.form-header h4{
    margin:10px 0px 15px;
    font-weight:600;
}
.btn{
    float: right;
    color: white;
    background: blue;
    border: 0;
    padding:8px 22px;
    border-radius: 4px;
}
#eye-icon{
    position: absolute;
    right: 12px;
    top: 10px;
    font-size: 22px;
    cursor: pointer;
}
a{
    text-decoration: none;
}
input{
  outline: #1a73e8;
}
#image{
  position: absolute;
  right:400px;
  top:100px;
}
#txt{
  position: absolute;
    right: 380px;
    top: 329px;
    width:270px
}
  </style>
</head>
<body>
  
<div class="container border rounded my-5">
  <div class="row">
        <div class="col-md-12 py-4 mx-2">
        <img src="https://upload.wikimedia.org/wikipedia/commons/2/2f/Google_2015_logo.svg" width="75">
        </div>
  </div>
  <div class="row">
        <div class="col-md-12 mx-2">
            <h1 id="heading">Create your Google Account</h1>
        </div>
  </div>
  <div id="image"><img src="https://ssl.gstatic.com/accounts/signup/glif/account.svg" alt="" width="244" height="244" class="j9NuTc TrZEUc"></div><div class="col-md-3 text-center" id="txt">One account. All of Google working for you.</div>

          <form action="" method="post">
               <div class="row">
           <div class="form-group col-md-3 my-2">
                    <input type="text input" id="email">
                    <label class="form-control-placeholder" for="name">Firstname</label>
                </div>
                <div class="form-group col-md-3 my-2">
                    <input type="text input" id="email">
                    <label class="form-control-placeholder" for="name">Lastname</label>
                </div>
           </div>   
           <div class="row">
           
                <div class="form-group col-md-6 my-2">
                    <input type="text input" id="email">
                    <label class="form-control-placeholder border-right-0" for="name">Username</label>
                </div>
           </div>  
           <div class="row">
           <div class="form-group col-md-3 my-2">
                    <input type="text input" id="password">
                    <label class="form-control-placeholder" id="pass" for="name">Password</label>
                </div>
                <div class="form-group col-md-3 my-2">
                    <input type="text input" id="confirmpassword">
                    <label class="form-control-placeholder" for="name" id="confirmpass">ConfirmPassword</label>
                </div>
           </div>  
           <div class="row my-2">
             <div class="col-md-12 ">
             <input type="checkbox" id="showPassword" />
                   <label for="showPassword">Show password</label>
             </div>
           </div>
           <div class="row ">
             <div class="col-md-3">
                  <a href="signin.php " class="btn btn-primary float-left  my-2" width="100px" href="signin.php" onclick="redirect()">Sign in instead</a>
             </div>
             <div class="col-md-3">
                    <button class="btn btn-primary float-right  mt-2" role="submit">Next</button>
             </div>
           </div>
          </form>
</div>
<script>
  document.getElementById('showPassword').onclick = function() {
    if ( this.checked ) {
       document.getElementById('password').type = "text";
       document.getElementById('confirmpassword').type = "text";
       document.getElementById("pass").style.display = 'none';
       document.getElementById("confirmpass").style.display = 'none';
    } else {
       document.getElementById('password').type = "password";
       document.getElementById('confirmpassword').type = "password";
    }
};

redirect=()=>{
    window.location.href('signin.php');
}
</script>
</body>
</html>